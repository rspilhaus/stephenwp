<?php

// -----------------------------------------------------------------------------
//! Include Libraries
// -----------------------------------------------------------------------------

	require_once 'functions/extended-cpts/extended-cpts.php';
	require_once 'functions/extended-taxos/extended-taxos.php';	
		
// -----------------------------------------------------------------------------
//! Function Partials
// -----------------------------------------------------------------------------	

	foreach( glob(TEMPLATEPATH . '/functions/{functions-}*.php', GLOB_BRACE ) as $functions_file ) {
		include $functions_file;
	}
	
