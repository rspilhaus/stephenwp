<?php get_header(); while ( have_posts() ) : the_post(); ?>


<div class="hero">
	<div class="container">
		<h1>Resource: <?php the_title(); ?></h1>
	</div>
</div>

<div class="main">
	<div class="container">
		<?php the_content(); ?>
	</div>
</div>		

<?php endwhile; get_footer(); ?>
