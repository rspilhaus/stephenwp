<?php get_header() ?>

<div class="hero">
	<div class="container">
		<h1>Archives</h1>
	</div>
</div>

<div class="blog">
	<div class="container">
		<?php if(have_posts()) : while ( have_posts() ) : the_post(); ?>
		<div class="blog-grid">
			
			<?php
				if ('' != get_the_post_thumbnail()){ 
					$urlArray = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'blog');
					$heroImg = "background-image:url(". $urlArray[0] .")";  
				} else {
					$heroImg = "";
				}	
			?>
		
			<a href="<?php the_permalink(); ?>" class="blog-box">
				<div class="blog-box-image" style="<?php echo $heroImg; ?>"></div>
				<div class="blog-box-text">
					<div class="blog-box-title"><?php the_title(); ?></div>
					<div class="blog-box-cat"><?php 
						$typeCount = 1;
						$types = get_the_terms( null, 'category' );
						if($types){
							foreach( $types as $type ) {
								if($typeCount > 1){
									echo ', ';
								}
								echo $type->name;													
								$typeCount++;
							}
						}
					?>
					</div>
					<div class="blog-box-excerpt"><?php echo get_excerpt(125); ?></div>
				</div>
			</a>
		</div>
		<?php endwhile; ?>
			<nav class="pagination">
				<?php 
					if (function_exists("pagination")) {
						pagination($wp_query->max_num_pages);
					} 
				?>
			</nav>
		<?php endif; ?>
		
	</div>
</div>	

<?php get_footer(); ?>
