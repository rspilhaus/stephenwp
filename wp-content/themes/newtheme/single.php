<?php get_header(); while ( have_posts() ) : the_post(); ?>

<?php
if ('' != get_the_post_thumbnail()){ 
	$urlArray = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'hero');
	$heroImg = "background-image:url(". $urlArray[0] .")";  
} else {
	$heroImg = "";
}	
?>
<div class="hero" style="<?php echo $heroImg; ?>">
	<div class="container">
		<h1><?php the_title(); ?></h1>
	</div>
</div>

<div class="main">
	<div class="container">
		
		<div class="blog-single-category">
			<?php 
				$typeCount = 1;
				$types = get_the_terms( null, 'category' );
				if($types){
					foreach( $types as $type ) {
						if($typeCount > 1){
							echo ', ';
						}
						echo '<a href="/category/' . $type->slug . '">';
						echo $type->name;			
						echo '</a>';										
						$typeCount++;
					}
				}
			?>
		</div>
		
		<?php the_content(); ?>
	</div>
</div>		

<?php endwhile; get_footer(); ?>
