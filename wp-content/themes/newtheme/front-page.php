<?php get_header(); ?>

<?php
	if(get_field('hero_image')) {
		$urlArray = wp_get_attachment_image_src(get_field('hero_image'), 'hero');
		$heroImg = "background-image:url(". $urlArray[0] .")";  
	} else {
		$heroImg = "";
	}	
?>

<div class="hero" style="<?php echo $heroImg; ?>">
	<div class="container">
		<?php if(get_field('hero_title')) : ?>
			<h1><?php the_field('hero_title'); ?></h1>
		<?php endif; ?>
		<?php if(get_field('hero_cta')) : ?>
			
			<?php $link = get_field('hero_cta'); ?>
			
			<a href="<?php echo $link['url']; ?>" class="btn" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		
		<?php endif; ?>
	</div>
</div>

<?php if( have_rows('subhero_boxes') ): ?>
<div class="subhero">
	<div class="container">
		<div class="subhero-grid">
			<?php 
				while( have_rows('subhero_boxes') ): the_row();
				$title = get_sub_field('title'); 				
			?>
				<div class="subhero-grid-box">
					<h3><?php echo $title; ?></h3>
					<p><?php echo get_sub_field('text'); ?></p>
				</div>
			<?php endwhile; ?>
		</div>
	</div>	
</div>
<?php endif; ?>


<div class="main">
	<div class="container">
		<?php the_field('main_content'); ?>
	</div>
</div>


<div class="resources">
	<div class="container">
		
		<div class="subhero-grid">
		<?php
		$args = array(
			'post_type' => 'resources', 
			'posts_per_page' => 6, 
			'orderby' => 'menu_order', 
			'order' => 'ASC'
		);
		$wp_query = new WP_Query($args);
			if($wp_query->have_posts()) : 
			while($wp_query->have_posts()) : 
			$wp_query->the_post();
		?>	
		
			<a class="subhero-grid-box" href="<?php the_permalink(); ?>">
				<?php the_title(); ?>
			</a>
		
		<?php endwhile; else: ?>
		<?php endif; wp_reset_query(); ?>
		
				
			
		</div>
		
	</div>	
</div>


<?php get_footer(); ?>
