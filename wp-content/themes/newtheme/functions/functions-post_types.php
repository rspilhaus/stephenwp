<?php 

// -----------------------------------------------------------------------------
//! Using Extended CPTs for quick + easy custom post types
//! See https://github.com/johnbillion/extended-cpts/wiki for documentation
// -----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//! People
//-----------------------------------------------------------------------------


register_extended_post_type( 'resources', array(
	
	# Change Title Prompt
	'enter_title_here' => 'Resource Name',
	
	# Change Menu Icon
	'menu_icon' => 'dashicons-groups',
	
	#Hierarchical?
	'hierarchical' => true,

	# Archive?
	'has_archive' => false,
	
	# Public?
	'public' => true,
	'show_in_nav_menus' => true,

	
), array(

    # Override the base names used for labels:
    'singular' => 'Resource',
    'plural'   => 'Resources',
    'slug'     => 'resources',
    
));


// -----------------------------------------------------------------------------
//!  Design Package Types
// -----------------------------------------------------------------------------

register_extended_taxonomy( 'resourceType', 'resources', array(

	#Sanitize to fix issue w/ array_map() when saving - necessary for Dropdown meta boxes
	'meta_box_sanitize_cb' => 'taxonomy_meta_box_sanitize_cb_input',

	# Show this taxonomy in the 'At a Glance' dashboard widget:
	'dashboard_glance' => false,

	# Add a custom column to the admin screen:
	'admin_cols' => array(
		'updated' => array(
			'title'       => 'Updated',
			'meta_key'    => 'updated_date',
			'date_format' => 'd/m/Y'
		),
	),

	'show_in_rest' => true,

), array(

	# Override the base names used for labels:
	'singular' => 'Resource Type',
	'plural'   => 'Resource Types',
	'slug'     => 'resource-type'

) );