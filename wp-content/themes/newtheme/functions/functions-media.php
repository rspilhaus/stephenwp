<?php

// -----------------------------------------------------------------------------
//! Featured Images
// -----------------------------------------------------------------------------

add_theme_support('post-thumbnails', array('page', 'post') );


// -----------------------------------------------------------------------------
//! Image Sizes
// -----------------------------------------------------------------------------

add_image_size( 'hero', 1440, 600, true);
add_image_size( 'blog', 340, 200, true);


// -----------------------------------------------------------------------------
//! Use Relative Image Links instead of full path when uploading
// -----------------------------------------------------------------------------

add_filter('get_image_tag', 'theme_get_image_tag');
function theme_get_image_tag($html)
{
    return str_replace(get_bloginfo('url'), '', $html);
}


// -----------------------------------------------------------------------------
//! Stop compressing Wordpress
// -----------------------------------------------------------------------------

add_filter('jpeg_quality', function($arg){return 100;});
add_filter('wp_editor_set_quality', function($arg){return 100;});

// -----------------------------------------------------------------------------
//! Remove image links by default
// -----------------------------------------------------------------------------

function atmo_imagelink_setup() {
    $image_set = get_option( 'image_default_link_type' );
     
    if ($image_set !== 'none') {
        update_option('image_default_link_type', 'none');
    }
}
add_action('admin_init', 'atmo_imagelink_setup', 10);

