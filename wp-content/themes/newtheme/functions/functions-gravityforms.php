<?php 
	
	add_filter( 'gform_ajax_spinner_url', 'ns_io_custom_gforms_spinner' );
	/**
	 * Changes the default Gravity Forms AJAX spinner.
	 *
	 * @since 1.0.0
	 *
	 * @param string $src  The default spinner URL.
	 * @return string $src The new spinner URL.
	 */
	function ns_io_custom_gforms_spinner( $src ) {
	
	    return get_stylesheet_directory_uri() . '/build/svg/ajax-loader.svg';
	    
	}
	
	
	add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
	function form_submit_button( $button, $form ) {
	    return "<button class='btn gform_button btn-color' id='gform_submit_button_{$form['id']}'><div class='btn-color-wrap'>Submit</div></button>";
	}