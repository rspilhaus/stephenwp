<?php
// -----------------------------------------------------------------------------
//! Remove Less-than-useful Dashboard Widgets
// -----------------------------------------------------------------------------

	add_action( 'wp_dashboard_setup', 'remove_default_dashboard_widgets' );
	function remove_default_dashboard_widgets() {
		global $wp_meta_boxes;
		remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
	}

// -----------------------------------------------------------------------------
//! Remove Some Sidebar Menu Items that are often confusing to clients
// -----------------------------------------------------------------------------


	function remove_menus_not_in_use(){

		//Remove Menus
		remove_menu_page( 'edit-comments.php' );      	//Comments

		//Remove Submenus
		$page = remove_submenu_page( 'themes.php', 'theme-editor.php' );

		// Remove Customize
		global $submenu;
		unset($submenu['themes.php'][6]);

	}
	add_action( 'admin_menu', 'remove_menus_not_in_use', 999 );


// -----------------------------------------------------------------------------
//! Remove Comments From Other Places
// -----------------------------------------------------------------------------

	// Removes from post and pages
	add_action('init', 'remove_comment_support', 100);

	function remove_comment_support() {
	    remove_post_type_support( 'post', 'comments' );
	    remove_post_type_support( 'page', 'comments' );
	}
	// Removes from admin bar
	function mytheme_admin_bar_render() {
	    global $wp_admin_bar;
	    $wp_admin_bar->remove_menu('comments');
	}
	add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );


// -----------------------------------------------------------------------------
//! Remove Some Admin Bar Items
// -----------------------------------------------------------------------------

	// Customize links
	add_action( 'wp_before_admin_bar_render', function() {
		global $wp_admin_bar;

		// Ditch the WP Links
		$wp_admin_bar->remove_menu( 'wp-logo' );


	} );



// -----------------------------------------------------------------------------
//! Login Customization
// -----------------------------------------------------------------------------

function my_login_logo() { ?>
    <style type="text/css">
	    .login {
		    background:#fff;
	    }
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/build/svg/logo.svg);
            background-size:164px 68px;
            width:164px;
            height:68px;
        }
        #login form {
	        background: #01B6C3;
	        border-radius: 4px;
	        box-shadow: none;
	        border: none;

        }
        #login form label {
	        color:#fff;
        }
        #login form input[type='text'],
		#login form input[type='password']{
			border-radius: 4px;
		}
		#login form input[type='text']:focus,
		#login form input[type='password']:focus{
			box-shadow: none;
			border: none;
		}
        #login form input[type='submit']{
	        color:#333;
	        background:#fff;
	        text-shadow: none;
	        border: none;
	        box-shadow: none;
        }
        #login #login_error, #login .message {
	        border-color:tomato;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );



