<?php

 // -----------------------------------------------------------------------------
 //! Scripts
 // -----------------------------------------------------------------------------

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);

function my_jquery_enqueue() {
	wp_deregister_script('jquery');
	wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js", false, null);
	wp_enqueue_script('jquery');
}


// -----------------------------------------------------------------------------
//! Load Scripts
// -----------------------------------------------------------------------------

function atmo_load_scripts() {
	
	wp_register_script( 'atmo_scripts', get_template_directory_uri() . '/build/js/theme.min.js', array('jquery'), filemtime(get_theme_file_path('/build/js/theme.min.js')), true  );
	wp_enqueue_script( 'atmo_scripts' );
	
}
add_action( 'wp_enqueue_scripts', 'atmo_load_scripts' );

// -----------------------------------------------------------------------------
//! Styles
// -----------------------------------------------------------------------------

function atmo_load_styles() {
	wp_register_style( 'atmo_styles', get_template_directory_uri() . '/build/css/theme.min.css', false, filemtime(get_theme_file_path('/build/css/theme.min.css')), 'all' );
	wp_enqueue_style( 'atmo_styles' );
}
add_action( 'wp_enqueue_scripts', 'atmo_load_styles' );


// -----------------------------------------------------------------------------
//! Remove Gutenberg Block Library CSS from loading on the frontend
// -----------------------------------------------------------------------------
function remove_wp_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
}
add_action( 'wp_enqueue_scripts', 'remove_wp_block_library_css', 100 );
