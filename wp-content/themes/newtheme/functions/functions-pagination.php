<?php 

// ---------------------------------------
//! Pagination
/*
	Place between Endwhile & Else in loop
	<?php if (function_exists('pagination')) {
		pagination($[wp_query_variable]->max_num_pages);
	} ?>
	
*/
// ---------------------------------------
	
function pagination($pages = '', $range = 2) {
	$showitems = ($range * 2)+1;  
	global $paged;
	
	if(empty($paged)) $paged = 1;
		
	if($pages == ''){
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages){
			$pages = 1;
		}
	}   
	
	if(1 != $pages){

		echo '<div class="pagination-block">';
		
		//Display Previous Link
		if($paged > 1){
			echo '<a title="Previous Page" class="previous pagination-button" href="'.get_pagenum_link($paged - 1).'"><svg width="7" height="9" viewBox="0 0 7 9" xmlns="http://www.w3.org/2000/svg"><path d="M0 4.5L7 0v9z" fill="#231F20" fill-rule="evenodd"/></svg></a>';
		}else {
			
		}
		if ($paged >= 4) {
			echo '<a href="'.get_pagenum_link(1).'" class=" first-page jump-number">1</a>';
			echo '<span class="pagination-ellipses">...</span>';
		}
		echo '</div>';
		
		echo '<div class="pagination-block pagination-numbers">';
		
		//Show Page Links/Nav
		for ($i=1; $i <= $pages; $i++){
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
				echo ($paged == $i)? '<span class="current">'.$i.'</span>':'<a href="' .get_pagenum_link($i).'" class="">'.$i.'</a>';
			}
		}
		echo '</div>';
		
		echo '<div class="pagination-block">';
		//Display Next Link
		if($paged <= $pages-3){
			echo '<span class="pagination-ellipses">...</span>';
			echo '<a href="'.get_pagenum_link($pages).'" class=" last-page jump-number">'.$pages.'</a>';
		}
		if ($paged < $pages) {
			echo '<a title="Next Page" class="next pagination-button" href="'.get_pagenum_link($paged + 1).'"><svg width="7" height="9" viewBox="0 0 7 9" xmlns="http://www.w3.org/2000/svg"><path d="M7 4.5L0 9V0z" fill="#231F20" fill-rule="evenodd"/></svg></a>';
		}else {
			
		}		
		echo '</div>';
	
	}
}
